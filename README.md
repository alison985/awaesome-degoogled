# awesome-degoogled
Awesome List (with recommendations) for how to De-Google Your Life

Recommendations based on:
* maintained, not deprecated
* Robust feature set 
* A connected, strong community 
* Code of conduct, inclusive values
* Extreme preference for public, git cloneable code
* Not just a different large corporations version

| Google Version | Recommended Alternative | Recommendation Reason | Additional Alternatives |
| ------ | ------ | ------ | ------ |
| ChromeOS |  |  |  |
| Android |  |  |  |
| Chrome Browser | Mozilla Firefox |  |
| Gmail |   |  |  |
| Google Drive |  |  | Dropbox | 
| Google Spreadsheet | [EtherCalc](https://ethercalc.net/) | Free, been around a long time | Lots, see Google Sheet Alternatives below. |
| Google Doc |   |  |  |
| Google Diagram |   |  |  |
| Google Slides |   |  |  |

## Google Sheet Alternatives
- Best Paid: [Airtable](https://airtable.com/) 
- Other Paid: [SmartSheet](https://www.smartsheet.com/) [Zoho Sheets](https://sheet.zoho.com/sheet/excelviewer) [Rows.com](https://rows.com/) [LagosOffice](https://www.lagooffice.com/en/index.html) | [OnlyOffice](https://www.onlyoffice.com/)
- Free and Paid?: [Sheets.js](https://sheetjs.com/)
- Untested: [Offidocs](https://www.offidocs.com/index.php/create-xls-online) [XMLGrid](https://xmlgrid.net/excelViewer.html) 

HAT TIP: [Mashable had the best list of online spreadsheet options](https://mashable.com/2008/02/05/forget-excel-14-online-spreadsheet-applications/)
